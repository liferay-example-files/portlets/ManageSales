<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@page import="com.liferay.portal.model.User" %>
<%@page import="com.liferay.portal.util.PortalUtil" %>

<%@page import="com.liferay.portal.service.GroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.persistence.UserFinder"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@page import="com.liferay.portal.model.User" %>
<%@page import="com.liferay.portal.util.PortalUtil" %>
<%@page import="com.liferay.portal.kernel.util.PrefsParamUtil" %>
<%@page import="com.liferay.portal.kernel.util.Validator" %>
<%@page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %>
<%@page import="com.liferay.portal.kernel.util.StringPool" %>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil" %>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil" %>

<%@page import="com.liferay.portlet.PortletPreferencesFactoryUtil" %>
<%@page import="com.liferay.portlet.PortletPreferencesFactory" %>

<%@page import="com.liferay.portlet.expando.model.ExpandoBridge"%>
<%@page import="com.liferay.portlet.expando.model.ExpandoTableConstants"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>

<%@page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@page import="com.liferay.util.portlet.PortletProps" %>

<%@page import="it.unisef.managesales.model.Worker" %>
<%@page import="it.unisef.managesales.model.Firm" %>

<%@page import="it.unisef.managesales.service.permission.ManageSalesPermission" %>
<%@page import="it.unisef.managesales.service.permission.WorkerPermission" %>
<%@page import="it.unisef.managesales.service.permission.FirmPermission" %>

<%@page import="it.unisef.managesales.service.util.ActionKeys" %>

<%@page import="it.unisef.managesales.WorkerNameException" %>
<%@page import="it.unisef.managesales.WorkerLastNameException" %>
<%@page import="it.unisef.managesales.FirmNameException" %>

<%@page import="it.unisef.managesales.service.FirmLocalServiceUtil" %>
<%@page import="it.unisef.managesales.service.WorkerLocalServiceUtil" %>
<%@page import="it.unisef.managesales.FirmNameException" %>

<%@page import="it.unisef.managesales.util.WebKeys" %>

<%@page import="java.text.Format" %>
<%@page import="java.util.Date" %>
<%@page import="java.util.List" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
	String currentURL = PortalUtil.getCurrentURL(request);
	String redirect = ParamUtil.getString(request,"redirect");


	Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat(
			"dd-MMM-yyyy", locale, timeZone);

%>

