<%@ include file="/init.jsp" %>

<%
	Firm firm =  (Firm) request.getAttribute(WebKeys.FIRM);
%>

<liferay-ui:header title='<%= firm != null ? firm.getName() : "new-firm" %>' backURL="<%= redirect %>" />

<portlet:actionURL name="updateFirm" var="updateFirmURL" >
	<portlet:param name="mvcPath" value="/edit_firm.jsp" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</portlet:actionURL>


<aui:form action="<%= updateFirmURL %>" method="post" name="fm" >
	<aui:fieldset>

		<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
		<aui:input type="hidden" name="firmId" value="<%= firm == null ? StringPool.BLANK : firm.getFirmId() %>" />

		<liferay-ui:error exception="<%= FirmNameException.class %>" message="please-enter-a-valid-name" />

		<aui:model-context bean="<%= firm %>" model="<%= Firm.class %>" />

		<aui:input name="name" />

		<aui:input name="address" />

	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" value="save" />

		<aui:button type="cancel" onClick="<%= redirect %>" />
	</aui:button-row>


</aui:form>