/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link it.unisef.managesales.service.http.WorkerServiceSoap}.
 *
 * @author    Giulio Piemontese
 * @see       it.unisef.managesales.service.http.WorkerServiceSoap
 * @generated
 */
public class WorkerSoap implements Serializable {
	public static WorkerSoap toSoapModel(Worker model) {
		WorkerSoap soapModel = new WorkerSoap();

		soapModel.setWorkerId(model.getWorkerId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setFirmId(model.getFirmId());
		soapModel.setEmploymentDate(model.getEmploymentDate());
		soapModel.setCode(model.getCode());

		return soapModel;
	}

	public static WorkerSoap[] toSoapModels(Worker[] models) {
		WorkerSoap[] soapModels = new WorkerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static WorkerSoap[][] toSoapModels(Worker[][] models) {
		WorkerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new WorkerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new WorkerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static WorkerSoap[] toSoapModels(List<Worker> models) {
		List<WorkerSoap> soapModels = new ArrayList<WorkerSoap>(models.size());

		for (Worker model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new WorkerSoap[soapModels.size()]);
	}

	public WorkerSoap() {
	}

	public long getPrimaryKey() {
		return _workerId;
	}

	public void setPrimaryKey(long pk) {
		setWorkerId(pk);
	}

	public long getWorkerId() {
		return _workerId;
	}

	public void setWorkerId(long workerId) {
		_workerId = workerId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public String getFirstName() {
		return _firstName;
	}

	public void setFirstName(String firstName) {
		_firstName = firstName;
	}

	public String getLastName() {
		return _lastName;
	}

	public void setLastName(String lastName) {
		_lastName = lastName;
	}

	public long getFirmId() {
		return _firmId;
	}

	public void setFirmId(long firmId) {
		_firmId = firmId;
	}

	public Date getEmploymentDate() {
		return _employmentDate;
	}

	public void setEmploymentDate(Date employmentDate) {
		_employmentDate = employmentDate;
	}

	public String getCode() {
		return _code;
	}

	public void setCode(String code) {
		_code = code;
	}

	private long _workerId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private long _groupId;
	private String _firstName;
	private String _lastName;
	private long _firmId;
	private Date _employmentDate;
	private String _code;
}