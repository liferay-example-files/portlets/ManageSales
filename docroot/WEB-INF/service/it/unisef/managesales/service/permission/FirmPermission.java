package it.unisef.managesales.service.permission;

import it.unisef.managesales.model.Firm;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.FirmLocalServiceUtil;
import it.unisef.managesales.service.WorkerLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.security.permission.PermissionChecker;

/* Azioni sui singoli task */

public class FirmPermission {

	public static void check(
			PermissionChecker permissionChecker, Firm firm,
			String actionId)
		throws PortalException {

		if (!contains(permissionChecker, firm, actionId)) {
			throw new PrincipalException();
		}
	}

	public static void check(
			PermissionChecker permissionChecker, long firmId, String actionId)
		throws PortalException, SystemException {

		if (!contains(permissionChecker, firmId, actionId)) {
			throw new PrincipalException();
		}
	}

	public static boolean contains(
		PermissionChecker permissionChecker, Firm firm,
		String actionId) {

		if (permissionChecker.hasOwnerPermission(
				firm.getCompanyId(), Firm.class.getName(),
				firm.getFirmId(), firm.getUserId(), actionId)) {

			return true;
		}


		return permissionChecker.hasPermission(
			firm.getGroupId(), Worker.class.getName(), firm.getFirmId(),
			actionId);
	}

	public static boolean contains(
			PermissionChecker permissionChecker, long firmId, String actionId)
		throws PortalException, SystemException {

		Firm firm = FirmLocalServiceUtil.getFirm(firmId);

		return contains(permissionChecker, firm, actionId);
	}

}
