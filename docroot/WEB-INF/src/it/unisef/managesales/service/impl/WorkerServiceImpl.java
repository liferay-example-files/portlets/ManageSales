/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ServiceContext;

import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.base.WorkerServiceBaseImpl;
import it.unisef.managesales.service.permission.FirmPermission;
import it.unisef.managesales.service.permission.WorkerPermission;
import it.unisef.managesales.service.util.ActionKeys;

/**
 * The implementation of the worker remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.managesales.service.WorkerService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.managesales.service.base.WorkerServiceBaseImpl
 * @see it.unisef.managesales.service.WorkerServiceUtil
 */
public class WorkerServiceImpl extends WorkerServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.managesales.service.WorkerServiceUtil} to access the worker remote service.
	 */

	public Worker addWorker(long userId, long firmId,
			String firstName,
			String lastName,
			String code,
			int employmentDateMonth,
			int employmentDateDay,
			int employmentDateYear,
			ServiceContext context)
				throws PortalException, SystemException {

		// Check da parte di Controllo (solleva un'eccezione), Contains da jsp
		FirmPermission.check(getPermissionChecker(), firmId,
									ActionKeys.ADD_WORKER );

		return workerLocalService.addWorker(userId, firmId, firstName, lastName,
				code, employmentDateMonth, employmentDateDay,
				employmentDateYear, context);
	}

	public Worker updateWorker(
			long workerId, String firstName,
			String lastName,
			String code,
			long firmId,
			int employmentDateMonth,
			int employmentDateDay,
			int employmentDateYear,
			ServiceContext context)
				throws PortalException, SystemException {

		WorkerPermission.check(getPermissionChecker(), firmId,
				ActionKeys.UPDATE);

		return workerLocalService.updateWorker(workerId, firstName, lastName,
				code, firmId, employmentDateMonth, employmentDateDay,
				employmentDateYear, context);

	}

	public Worker deleteWorker(long workerId)
				throws PortalException, SystemException {

		WorkerPermission.check(getPermissionChecker(), workerId,
				ActionKeys.DELETE);

		return workerLocalService.deleteWorker(workerId);
	}
}