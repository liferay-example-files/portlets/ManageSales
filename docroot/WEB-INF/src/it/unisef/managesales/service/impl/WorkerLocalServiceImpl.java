/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.service.impl;

import it.unisef.managesales.WorkerLastNameException;
import it.unisef.managesales.WorkerNameException;
import it.unisef.managesales.model.Worker;
import it.unisef.managesales.service.base.WorkerLocalServiceBaseImpl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;

/**
 * The implementation of the worker local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link it.unisef.managesales.service.WorkerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Giulio Piemontese
 * @see it.unisef.managesales.service.base.WorkerLocalServiceBaseImpl
 * @see it.unisef.managesales.service.WorkerLocalServiceUtil
 */

public class WorkerLocalServiceImpl extends WorkerLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link it.unisef.managesales.service.WorkerLocalServiceUtil} to access the worker local service.
	 */

	public Worker addWorker(long userId, long firmId,
			String firstName,
			String lastName,
			String code,
			int employmentDateMonth,
			int employmentDateDay,
			int employmentDateYear,
			ServiceContext context)
				throws PortalException, SystemException {

			// Non richiamare i metodi di LocalServiceUtil, non serve

		User user = userPersistence.findByPrimaryKey(userId);

		validate(firstName, lastName);

		long WorkerId = counterLocalService.increment();

		// Creazione del Bean

		Worker worker = workerPersistence.create(WorkerId);

		worker.setCompanyId(user.getCompanyId());
		worker.setUserId(user.getUserId());
		worker.setGroupId(context.getScopeGroupId());
		worker.setUserName(user.getFullName());


		Calendar employmentDate = CalendarFactoryUtil.getCalendar();

		employmentDate.set(Calendar.MONTH, employmentDateMonth);
		employmentDate.set(Calendar.DAY_OF_MONTH, employmentDateDay);
		employmentDate.set(Calendar.YEAR, employmentDateYear);
		employmentDate.set(Calendar.HOUR_OF_DAY, 0);
		employmentDate.set(Calendar.MINUTE, 0);
		employmentDate.set(Calendar.SECOND, 0);


		worker.setCreateDate(new Date());
		worker.setModifiedDate(new Date());

		worker.setFirstName(firstName);
		worker.setLastName(lastName);
		worker.setCode(code);

		worker.setFirmId(firmId);

		worker.setEmploymentDate(employmentDate.getTime());

		// La WorkerPersistence può essere solo richiamata dalla LocalServiceImpl
		worker = workerPersistence.update(worker, false); // false
													// non fa il merge dei dati che eventualmente gi� ci sono

//		resourceLocalService.addModelResources(
//		worker.getCompanyId(), worker.getGroupId(), worker.getUserId(),
//		worker.class.getName(), worker.getWorkerId(), serviceContext.getGroupPermissions(),
//		serviceContext.getGuestPermissions());


		return worker;
	}


	public Worker updateWorker(
			long workerId, String firstName,
			String lastName,
			String code,
			long firmId,
			int employmentDateMonth,
			int employmentDateDay,
			int employmentDateYear,
			ServiceContext context)

		throws PortalException, SystemException {

		Worker worker = workerPersistence.findByPrimaryKey(workerId);

		validate(firstName, lastName);

		Calendar employmentDate = CalendarFactoryUtil.getCalendar();

		employmentDate.set(Calendar.MONTH, employmentDateMonth);
		employmentDate.set(Calendar.DAY_OF_MONTH,  employmentDateDay);
		employmentDate.set(Calendar.YEAR, employmentDateYear);
		employmentDate.set(Calendar.HOUR_OF_DAY, 0);
		employmentDate.set(Calendar.MINUTE, 0);
		employmentDate.set(Calendar.SECOND, 0);

		worker.setEmploymentDate(employmentDate.getTime());

		worker.setFirstName(firstName);
		worker.setLastName(lastName);
		worker.setCode(code);
		worker.setFirmId(firmId);

		worker.setGroupId(context.getScopeGroupId());

		worker.setModifiedDate(new Date());

		worker = workerPersistence.update(worker, false);

		return worker;

	}

	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public Worker deleteWorker(long workerId) throws PortalException,
			SystemException {

		return super.deleteWorker(workerId);
	}

	protected void validate(String firstName, String lastName) throws PortalException {

		if(Validator.isNull(firstName)) {
			throw new WorkerNameException();
		}

		if(Validator.isNull(lastName)) {
			throw new WorkerLastNameException();
		}

	}


	public List<Worker> getWorkers(long companyId, long groupId, int start, int end)
			throws SystemException {

			return workerPersistence.findByC_G(companyId, groupId, start, end);
	}

	public int getWorkersCount(long companyId, long groupId)
			throws SystemException {

			return workerPersistence.countByC_G(companyId, groupId);
	}

	public void deleteWorkersByFirm(long firmId)
			throws SystemException {

		List<Worker> firmWorkers = workerPersistence.findByFirmId(firmId);

		for(Worker worker : firmWorkers ) {

			workerPersistence.remove(worker);

		}

	}
}