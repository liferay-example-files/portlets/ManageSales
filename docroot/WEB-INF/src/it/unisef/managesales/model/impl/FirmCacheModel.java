/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.unisef.managesales.model.Firm;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Firm in entity cache.
 *
 * @author Giulio Piemontese
 * @see Firm
 * @generated
 */
public class FirmCacheModel implements CacheModel<Firm>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(19);

		sb.append("{firmId=");
		sb.append(firmId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", name=");
		sb.append(name);
		sb.append(", address=");
		sb.append(address);
		sb.append("}");

		return sb.toString();
	}

	public Firm toEntityModel() {
		FirmImpl firmImpl = new FirmImpl();

		firmImpl.setFirmId(firmId);
		firmImpl.setCompanyId(companyId);
		firmImpl.setUserId(userId);

		if (userName == null) {
			firmImpl.setUserName(StringPool.BLANK);
		}
		else {
			firmImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			firmImpl.setCreateDate(null);
		}
		else {
			firmImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			firmImpl.setModifiedDate(null);
		}
		else {
			firmImpl.setModifiedDate(new Date(modifiedDate));
		}

		firmImpl.setGroupId(groupId);

		if (name == null) {
			firmImpl.setName(StringPool.BLANK);
		}
		else {
			firmImpl.setName(name);
		}

		if (address == null) {
			firmImpl.setAddress(StringPool.BLANK);
		}
		else {
			firmImpl.setAddress(address);
		}

		firmImpl.resetOriginalValues();

		return firmImpl;
	}

	public long firmId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String name;
	public String address;
}