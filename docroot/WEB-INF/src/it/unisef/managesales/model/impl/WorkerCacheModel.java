/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managesales.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import it.unisef.managesales.model.Worker;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Worker in entity cache.
 *
 * @author Giulio Piemontese
 * @see Worker
 * @generated
 */
public class WorkerCacheModel implements CacheModel<Worker>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(25);

		sb.append("{workerId=");
		sb.append(workerId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", firstName=");
		sb.append(firstName);
		sb.append(", lastName=");
		sb.append(lastName);
		sb.append(", firmId=");
		sb.append(firmId);
		sb.append(", employmentDate=");
		sb.append(employmentDate);
		sb.append(", code=");
		sb.append(code);
		sb.append("}");

		return sb.toString();
	}

	public Worker toEntityModel() {
		WorkerImpl workerImpl = new WorkerImpl();

		workerImpl.setWorkerId(workerId);
		workerImpl.setCompanyId(companyId);
		workerImpl.setUserId(userId);

		if (userName == null) {
			workerImpl.setUserName(StringPool.BLANK);
		}
		else {
			workerImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			workerImpl.setCreateDate(null);
		}
		else {
			workerImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			workerImpl.setModifiedDate(null);
		}
		else {
			workerImpl.setModifiedDate(new Date(modifiedDate));
		}

		workerImpl.setGroupId(groupId);

		if (firstName == null) {
			workerImpl.setFirstName(StringPool.BLANK);
		}
		else {
			workerImpl.setFirstName(firstName);
		}

		if (lastName == null) {
			workerImpl.setLastName(StringPool.BLANK);
		}
		else {
			workerImpl.setLastName(lastName);
		}

		workerImpl.setFirmId(firmId);

		if (employmentDate == Long.MIN_VALUE) {
			workerImpl.setEmploymentDate(null);
		}
		else {
			workerImpl.setEmploymentDate(new Date(employmentDate));
		}

		if (code == null) {
			workerImpl.setCode(StringPool.BLANK);
		}
		else {
			workerImpl.setCode(code);
		}

		workerImpl.resetOriginalValues();

		return workerImpl;
	}

	public long workerId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public long groupId;
	public String firstName;
	public String lastName;
	public long firmId;
	public long employmentDate;
	public String code;
}